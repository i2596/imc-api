package com.imc.service;
import com.imc.domain.imc.Imc;
import com.imc.domain.imc.dto.ImcCalculateDto;
import com.imc.domain.imc.dto.ImcDto;
import com.imc.domain.imc.enums.Classification;
import com.imc.domain.imc.repository.ImcRepository;
import com.imc.web.validator.ValidatorImc;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
@AllArgsConstructor
@NoArgsConstructor
public class ImcService {


    ValidatorImc validatorImc;

    @Autowired
    ImcRepository repository;


    public ImcDto calculateImc(Double height, Double weight) {
        validatorImc = new ValidatorImc(height,weight);

        var valueCalculated = (weight / (height * height));
        return ImcDto.builder()
            .valueCalculated(valueCalculated)
            .classification(classificateImc(valueCalculated))
            .build();
    }

    private Classification classificateImc(Double valueCalculated) {
        if (valueCalculated < 18.5) {
            return Classification.LOW_WEIGHT;
        }
        if (valueCalculated >= 18.5 && valueCalculated <= 24.9) {
            return Classification.NORMAL;
        }
        if (valueCalculated >= 25 && valueCalculated <= 29.9) {
            return Classification.OVERWEIGHT;
        }

        return Classification.OBESITY;
    }

    public Imc saveImc(ImcCalculateDto imcDto) {
        var value= calculateImc(imcDto.getHeight(), imcDto.getWeight());
        Imc imc = Imc.builder()
                .height(imcDto.getHeight())
                .weight(imcDto.getWeight())
                .imcValue(value.getValueCalculated())
                .classification(value.getClassification())
                .build();

        return repository.save(imc);
    }
    @Transactional
    public void deleteImcById(Long id) {
        repository.deleteImcById(id);
    }




}
