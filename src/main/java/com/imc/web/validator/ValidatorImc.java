package com.imc.web.validator;

import java.util.Objects;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidatorImc {

    private Double height;
    private Double weight;
    private final Long INVALID_VALUE = Long.valueOf(600);

    public ValidatorImc(Double height, Double weight) {
        shouldValidateValues(height, weight);
        this.height = height;
        this.weight = weight;
    }

    private void shouldValidateValues(Double height, Double weight) {
        if (Objects.isNull(height) && Objects.isNull(weight)) {
            throw new RuntimeException("values not be null");
        }
        if (height >= INVALID_VALUE && weight >= INVALID_VALUE) {
            throw new RuntimeException("invalid values");
        }

    }

}
