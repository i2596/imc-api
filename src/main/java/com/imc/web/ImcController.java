package com.imc.web;

import com.imc.domain.imc.dto.ImcCalculateDto;
import com.imc.service.ImcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("imc")
public class ImcController {
    @Autowired
    ImcService imcService;

    @CrossOrigin
    @GetMapping("/calculate/height/{height}/weight/{weight}")
    public ResponseEntity calculate(@PathVariable Double height, @PathVariable Double weight){
        return ResponseEntity.ok(imcService.calculateImc(height,weight));
    }

    @CrossOrigin
    @PostMapping("/save")
    public ResponseEntity insertImc(@RequestBody ImcCalculateDto imcDto) {
        return ResponseEntity.ok(imcService.saveImc(imcDto));

    }
}
