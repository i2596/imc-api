package com.imc.domain.imc.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.Objects;

@Getter
@ToString
@AllArgsConstructor
public enum Classification {
    LOW_WEIGHT(1,"Low_Weight"),
    NORMAL(2,"Normal"),
    OVERWEIGHT(3,"Overweight"),
    OBESITY(4,"Obesity");

    private int code;

    private String description;

    public static Classification getClassification(String description) {
        if (Objects.nonNull(description)) {
            for (Classification classification : values()) {
                if (classification.getDescription().equalsIgnoreCase(description))
                    return classification;
            }
        }

        throw new IllegalArgumentException(description + " is not a classification valid!");
    }


}
