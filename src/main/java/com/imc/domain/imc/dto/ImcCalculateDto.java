package com.imc.domain.imc.dto;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ImcCalculateDto {
    private Double height;
    private Double weight;
}
