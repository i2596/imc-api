package com.imc.domain.imc.dto;

import com.imc.domain.imc.enums.Classification;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class ImcDto {

    private Double valueCalculated;
    private Classification classification;
}
