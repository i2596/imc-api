package com.imc.domain.imc;

import com.imc.domain.imc.enums.Classification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
@Builder
@AllArgsConstructor
public class Imc {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double height;

    private Double weight;

    private Double imcValue;

    @Enumerated(EnumType.STRING)
    private Classification classification;
}
