package com.imc.domain.imc.repository;
import com.imc.domain.imc.Imc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ImcRepository extends JpaRepository<Imc,Long> {

    @Modifying
    @Query("delete from Imc i where i.id= :id")
    void deleteImcById(Long id);

}
