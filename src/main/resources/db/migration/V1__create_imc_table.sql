CREATE TABLE imc (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    height REAL NOT NULL,
    weight REAL NOT NULL,
    classification VARCHAR NOT NULL
);