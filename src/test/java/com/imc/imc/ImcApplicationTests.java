package com.imc.imc;

import com.imc.domain.imc.dto.ImcCalculateDto;
import com.imc.service.ImcService;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import javax.transaction.Transactional;

@SpringBootTest
class ImcApplicationTests {

	@Autowired
	ImcService imcService;

	@Test
	void shouldCalculateValidValues() {
		assertEquals(50.390526581002774, imcService.calculateImc(1.89,180.0).getValueCalculated());
	}


	@Test
	void shouldTrhowsExceptionWhenArgumentsNull() {
		assertThrows(RuntimeException.class,() -> imcService.calculateImc(null,null));
	}

	@Test
	void shoudlValidateWhenInvalidArgumentsAreSend() {
		assertThrows(RuntimeException.class,() -> imcService.calculateImc(20000.0,50000.0));
	}

	@Test
	void shouldSaveImcWhenParametersAreValid() {
		ImcCalculateDto imcCalculateDto = ImcCalculateDto.builder()
				.height(1.80)
				.weight(70.0)
				.build();
		assertNotNull(imcService.saveImc(imcCalculateDto));
	}
	@Test
	@Transactional
	void shouldDeleteImc(){
		assertDoesNotThrow(() -> imcService.deleteImcById(1L));
	}
}
