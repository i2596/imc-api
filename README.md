
O objetivo desse projeto é ter as implementações referentes calculo de IMC. A base de dados será gerenciada por esse projeto

# Tecnologia

* Java 11 
* SpringBoot 2.6.3 
* Base de Dados PostgreSQL 
* Hibernate 
* Maven para gestão das dependências

## Run project

Construir o projeto 

* mvn clean install

Para rodar

*  mvn spring-boot:run
